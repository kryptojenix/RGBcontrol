/*
Controls RGB LED using presets or serial (now with i2c) data

 Name:		RGBcontrol.ino
 Created:	15/01/2017
 Author:	Ben Cox
 GNUPL 3.0+
 
 adapted from

 Name:		RGBLedPot.ino
 Created:	17/2/16 9:47:03 AM
 Author:	Ahmed Hamdy
 GNUPL 3.0+
 
CHANGE LOG
 20180604 -- re-structure
 20170806 -- trim list of variables to free up some memory (use byte instead of 3 character)
 20170806 -- use fadeMatrix for complex fades
 20170803 -- begin i2c implementation
 20170130 -- move EEPROM address to include ID in first x? bytes
 20170123 -- implement preset EEPROM storage#
 20170119 -- implement preset matrix
 20170119 -- recieve serial commands 
 20170115 -- removed potentiometer control
 20170115 -- corrected fade with float variables
 
 

TO DO 
20170808 -- clean up prefix filter, prefix clear or keep conditions, serial commands getting stuck in buffer
20170130 -- major clean up of serial sends
20170123 -- send current preset back to master when preset change in autoFade
         -- fade duration too long, make it update during fade
20170120 -- autoFade doesn't switch off until after the next fade, make it instant
         -- more error checking on inputString, variables after prefix, bad prefix

20170119 -- integrate presets with programmer via serial
         -- manual / preset modes via serial
         -- auto change / trigger option in preset mode
         -- fix p++ to not increase when a fade is sent, other cases...

         

    * serial commands 
        
        s = store these values as the current preset
        n = next preset (but don't fade yet)
        m = change mode preset / live
        a = toggle preset autoFade
        p = change to this preset number
        f = fade duration multipliers[100,100,100]
        x = manual RGB mode [255,255,255]
    
        
        
    * eeeprom addresses
         
*/

                                // load libraries
#include<EEPROM.h>              // get access to the eeprom to store & recall ID and preset values
#include <Wire.h>               // use the i2c interface for communications

                                // global starting values
boolean mode = 1;               // 1=preset or 0=manual mix mode
boolean autoFade = 1;           // 1=autoFade 0=wait for input
//boolean fadeNow = 0;          // 1 if we're ready to fade on this loop
boolean debug = 1;


const int fadeSteps = 25;       // used to calculate step values 255 max resolution (lower for fast fades)
int fadeType = 2;               // The type of to calculate: 1 = ?   2 = ?
const int EEidReserved = 1;     // number of bytes reserved for uCID (Microcontroller ID) for starting i2c (slave ID)
const int EEsize=1024;          // size in bytes of the board's eprom (AT328)
int EEpos=0;                    // position of a byte in the EEPROM - used for iterating through addresses

int fadeDuration = 20;          // input how long to make the fade last (multiplier 5ms per colour)
int holdDuration = 100;         // input squared for holdTime (1msec - 1min)
int fadeType = 1;               // input fadeType byte sized, maybe use 0 - 9 for now.
int fadeStats[3];               // three variables {fadeDuration,holdDuration,fadeType}

fadeStats = {fadeDuration*5,holdDuration^2,fadeType};

int preset[7][3];       //define the matrix of presets to be filled from or written to eeprom

// setup the pins
int ledPin = 13;
int outRGB[] = { 3,5,6 };   // PWM output pins to control brightness of each color in the RGB LED

//setup global variables
float current[3];           // array that holds the current RGB values for fade calculations
float target[3];            // array that holds the target preset values for fade calculations
int p = 0;                  // the current preset number
int pMax = 6;               // stores the total number of presets for cycling

                            // start presets at the 4th byte of EEPROM after a 3 byte ID
int uCID;                   // ID of this device. Written to EEPROM by the programmer
int uCIDtemp;               // ID of the incoming serial message *obsoleted by i2c
String uCIDstring;          // for converting string to int, use varies

char tempChar;              //to extract numeric characters from inputString
String tempString;          //to extract numeric characters from inputString

//float increment[3];       //array to store the fade increments for each colour ** floating point calcs, no negative numbers!!!!
float fade[3];              //array stores the value for each colour throughout the fade
int diff[3];                //store the difference as an integer to get negative numbers 

byte fadeMatrix[3][fadeSteps];    //store the complete fade as a matrix of 256 steps, RGB

String inputString = "";         // a string to hold incoming data
//boolean stringComplete = false;  // whether the string is complete (used to flag an i2c event recieved)
// char prefix;

void setup() {
  
    Serial.begin(9600);
    //delay(1000);

    inputString.reserve(10);                // reserve 10 bytes for the inputString

    //************************ WRITE uCID TO EEEPROM *************************//  
    // EEPROM.write(0,0);   // single byte in the first address position (address, value)
    //************ comment out after first run **************//

    
    //********* write the test pallette to eeeprom or restore saved pallette *********//
    // restoreDefaultPallette();
    restoreSavedPallette();
    //************************ comment out after first run ***************************//
    
    
    
    //read the EEid from the first position of EEPROM
    Serial.print("uCID=");
    uCID = int(EEPROM.read(0));
    
    Serial.println();
    Serial.println("Start i2c");
    // start I2C using the uCID
    Wire.begin(uCID);                // join i2c bus with address
    Wire.onReceive(receiveEvent);       // register event
    
    //read the presets from the eeprom
    EEpos=EEidReserved+1;
    for (p=0; p < (pMax+1); p++){
        for (int i=0; i<3; i++){
            Serial.print("EEpos :");
            Serial.println(EEpos);
            preset[p][i]=EEPROM.read(EEpos);
            EEpos++;
        }
    }

    pinMode(ledPin, OUTPUT);
    // Loop on all pins ( 3 values: Red, Green and Blue )
    for (int i = 0; i < 3; i++){
        pinMode(outRGB[i], OUTPUT);	// Prepare those pins to output the values of the RGB LED
    }
    
    // ------- for setup preset[0][] is already defined as 255,255,255 (white)               
 
    //load the preset[0] which is white
    //this preset will not be used in the cycle but can be called later
    // don't know if I'll keep it like this, it makes working with presets easier 
    // because they can start at 1    
    
    Serial.print("START ");
    for (int i = 0; i < 3; i++){
        //Serial.print(preset[p][i]);
        //Serial.print(" ");
        analogWrite(outRGB[i],preset[0][i]);         // writes to RGB output pins
    }
    Serial.println("");
    Serial.print("MODE=");
    Serial.print(mode);
    if(mode==0){
        Serial.println(" mix");
    }
    else Serial.println(" preset");
    Serial.print("AUTOFADE=");
    Serial.println(autoFade);
    //print the presets to serial
    for (int p=0; p < (pMax+1); p++){
        Serial.print(p);
        Serial.print("    ");
        for (int i=0; i<3; i++){
        Serial.print(preset[p][i]);
        Serial.print(" ");
        }
        Serial.println("");
    }
    p=0;
            
}

void restoreDefaultPallette(){

    Serial.println("Writing test pallette to EEPROM");
    // the following is the test pallette definition, 
    // uncomment restoreDefaultPallette in void setup() to save this to eeprom
    int preset[7][3] = {  255,  255,  255,
                            255,  0,    0,
                            0,    255,  0,
                            123,  0,    255,
                            255,  123,  0,
                            0,    255,  123,
                            123,  0,    255};

    // write the test pallette to eeprom
    writeCurrentPallette();
}


void restoreSavedPallette(){

        //preset[p][0]=tempR.toInt();
        //preset[p][1]=tempG.toInt();
        //preset[p][2]=tempB.toInt();

        // some error correction after values are written into preset matrix
        // then preset is written to EEPROM at the correct position EEpos



}


void writeCurrentPallette(){

    for(p=0; p < pMax; p++){
        Serial.print(p);
        Serial.print("    ");

        for(int i = 0; i < 3; i++){
            EEpos=EEidReserved+(p*3)+i+1;
            
            Serial.print(",   ");
            Serial.print(preset[p][i]);

            if(preset[p][i] > 255){
            preset[p][i]=255;
            }
        //Serial.print("  ");  

        // ! write line
        EEPROM.write(EEpos,preset[p][i]);
        // ! write line

        }
        Serial.println();
    }

}    //************** end of write to eeprom *************************






//********* i2c function that executes whenever data is received from master ******//
void receiveEvent(int junk) {
    Serial.println(junk);
    while (Wire.available()) { // loop through all characters in the message
        char c = Wire.read(); // receive byte as a character
        inputString += c;
        //Serial.print(c);  
    }
    //Serial.println();  
    //Serial.println(inputString);
    inputRec();
    //return(inputString);
  
}

//***************  Input Received - decode the message, set some variables ******************//
void inputRec(){
  
    /* read the first character prefix
            decide what to do.....  s = store these values as the current preset
                                    n = next preset (but don't fade yet)
                                    m = change mode preset / live
                                    a = toggle preset autoFade
                                    p = change to this preset number
                                    f = fade duration multipliers[100,100,100]
    */

    char prefix=inputString.charAt(EEidReserved);
    Serial.println(inputString);
    Serial.print("Prefix=");
    Serial.println(prefix);

//prefixes that need to be dealt with early are done here, 

    switch(prefix){

        case'd':
            debug=!debug;
            //prefix=0;
            inputString=0;
            break;

        case'a':
            tempString=inputString.substring(1+EEidReserved);
            autoFade=tempString.toInt();
            Serial.print("changing autoFade to ");
            Serial.println(autoFade);
            //fadeNow=autoFade;  //if autoFade is off, don't fade this loop
            //prefix=0;
            inputString=0;
            break;
        
        case'm':
            tempString=inputString.substring(1+EEidReserved);
            mode=tempString.toInt();
            Serial.print("changing mode to ");
            if(mode==0){
                Serial.println("manual");
            }
            if(mode==1){
                Serial.println("preset");
            }
                    
            //fadeNow=0;
            prefix=0;
            inputString=0;
            break;
        
        case'x':
            if(mode==1){ //ignore x messages unless in mode==0 manual mode
                prefix=0;
                inputString=0;
                break;
            }
        case's':
            if(mode==1){ //ignore x messages unless in mode==0 manual mode
                prefix=0;
                inputString=0;
                break;
            }
    }
    
//~~~~~~~~~~ end of early prefix processing (not mode specific) ~~~~~~~~~~~~~~~~~~~//
//=============================== mode==0 (manual mode) ===========================//
    if (mode==0){
        
        Serial.println(prefix);
        
        switch(prefix){
            //----------------X = manual RGB value ---------------//
            case 'x':
                for(int i=0; i<3; i++){
                    // TODO: read the variables from the string before the switches 
                    tempString=inputString.substring((i*3)+1+EEidReserved,(i*3)+4+EEidReserved);
                    analogWrite(outRGB[i],tempString.toInt());
                    //need to write current at some point, when exiting manual mode?
                    //current[i]=tempString.toInt();
                }
                Serial.print("R ");
                Serial.print(current[0]);
                Serial.print("   G ");
                Serial.print(current[1]);
                Serial.print("   B ");
                Serial.println(current[2]);
                break;
                
            //----------------P = change to preset---------------//
            case 'p':
                tempString=inputString.substring(1+EEidReserved);
                p=tempString.toInt();
                Serial.print("change to preset ");
                Serial.println(p);
                for(int i = 0; i < 3; i++){
                    target[i]=int(preset[p][i]);
                }
                break;
            
            //---------------Store preset----------------------
            case 's':
                
                for(int i=0; i<3; i++){
                    tempString=inputString.substring((i*3)+1+EEidReserved,(i*3)+4+EEidReserved);
                    Serial.print(tempString);
                    preset[p][i]=tempString.toInt();
                    // some error correction before values are written into preset matrix
                    if(preset[p][i] > 255){
                    preset[p][i]=255;
                    }
                    // duplicate writes to current[] within inputRec
                    //current[i]=preset[p][i];
                    
                }
                                        
                // then preset is written to EEPROM at the correct position EEpos
                Serial.print("Writing Preset to EEprom. Preset = ");
                Serial.println(p);
                Serial.print("EEpos = ");
                
                for(int i = 0; i < 3; i++){
                EEpos=EEidReserved+(p*3)+i+1;
                Serial.print(EEpos);
                Serial.print(", ");
                
                
                EEPROM.write(EEpos,preset[p][i]);
                break;
                }        
                        // make the new preset current[] * done in case statements
                        // reset input global variables
            inputString=0;       
            prefix=0;
        }     
    }  //================================= end of mode==0 ======================================//

//============================== mode = 1 (Preset mode) ===========================//
    if (mode==1){
    
        switch(prefix){
            
            case'f':   //read the fade variables from inputString
                for(int i=0; i<3; i++){
                    tempString=inputString.substring((i*3)+1+EEidReserved,(i*3)+4+EEidReserved);
                    analogWrite(outRGB[i],tempString.toInt());
                    // fadeStats[3] are int values after the uCID = [int,int,int]
                    fadeStats[i]=tempString.toInt();
                }
                //some sanity checks?
                prefix=0;
                inputString=0;
                break;
                
            case 'p':   //----------------P = change to preset---------------//
                tempString=inputString.substring(1+EEidReserved);
                p=tempString.toInt();
                Serial.print("change to preset ");
                Serial.println(p);
                for(int i = 0; i < 3; i++){
                    target[i]=int(preset[p][i]);
                }
                // clear the string:
                inputString = 0;
                prefix =0;
                doFade();
                //fadeNow=0;
                break;
        }   //************************************ end of autoFade==0 *************************************//
               
    }       //==================================== end of mode==1 =========================================//

}           //************************************ end of void inputRec ***********************************//


//******************** doFade - fills the fadeMatrix and performs the fade ********************//
void doFade(){
    /* 
        1. Use current preset & target preset to calculate the increment for each step of the fade
        2. 
        If "darkFade" is true, we will fade through zero
    
    
    */
    //const int darkTime = 10;        // when darkFade = 1 - fade through zero and hold for this many steps
    //target[0]=targetR;
    //target[1]=targetG;
    //target[2]=targetB;
    
    // this is where the fadeType is processed and values are assigned
    int darkTime = 2;
    //int fadeSlope = 2;      //not implemented
    float increment[3] = {0,0,0};
    
    //for (int i = 0; i < 3; i++){
        
    //  fadeMatrix[i][0] = byte(current[i]);   // sends the starting values to the fadeMatrix          
    //}

    // print all the colour chanel values and
    Serial.print("Preset = ");
    Serial.print(p);
    Serial.print("/");
    Serial.print(pMax);
    Serial.print("        R ");
    Serial.print("           G ");
    Serial.print("           B ");
    Serial.println(""); 
    Serial.print("current            ");
    Serial.print(current[0]);
    Serial.print("          ");
    Serial.print(current[1]);
    Serial.print("          ");
    Serial.print(current[2]);
    Serial.println("");         
    
    Serial.print("target             ");
    Serial.print(target[0]);
    Serial.print("          ");
    Serial.print(target[1]);
    Serial.print("          ");
    Serial.print(target[2]);
    Serial.println("");
            
    // calculate the distance for each colour to fade 

    Serial.print("diff               ");
    for (int i = 0; i < 3; i++){
        // set diff[] for each colour chanel by calculating difference
        // if the difference is small use a longer fade algorithm
        diff[i] = target[i] - current[i];
        Serial.print(diff[i]);
        Serial.print("            ");
    }
    Serial.println("");
    
    fadeType=fadeStats[2];  //
    delay(500);
    //-----------calc steps and write the matrix----------------//
    
    int f=0;
    switch(fadeType){
    case 1:
        
        
        
    //******** calc fade down part ***********
    //Serial.println(millis());
    //int s=(fadeSteps - darkTime)/2;
        
    Serial.print("inc down           ");
    for (int i = 0; i < 3; i++){
        // set increment[] floating point 
        increment[i] = current[i] / ((fadeSteps - darkTime)/2);
        Serial.print(increment[i]);
        Serial.print("        ");
    }
    Serial.println();
    
    for (f=0; f < (fadeSteps - darkTime)/2; f++){
        if (debug){Serial.print("calculating....          ");}
        for (int i = 0; i < 3; i++){
            fade[i]=current[i]-increment[i];
            fadeMatrix[i][f]=fade[i];
            current[i]=fade[i];
            //delay(fadeStats[1]);
            if (debug){
                Serial.print(fade[i]);
                Serial.print("          ");
            } 
        }
        if (debug){Serial.println();
        }
    }
        
    //********* the dark pause *******
    Serial.println("dark pause");
    for (f=(fadeSteps - darkTime)/2; f < ((fadeSteps - darkTime)/2) + darkTime; f++){
        if (debug){Serial.print("calculating....          ");}
        for (int i = 0; i < 3; i++){
            fadeMatrix[i][f]=0;
            if (debug){
                Serial.print(fade[i]);
                Serial.print("          ");
            }
            current[i]=0;
            fade[i]=0;   //doesn't need to be done every loop
        }
        if (debug){
            Serial.println();  
        }
    }
        
        
        
    //********** the fade up part ********
    // calc a new increment[] value
    Serial.print("inc up             ");
    for (int i = 0; i < 3; i++){  
        increment[i]=target[i]/(fadeSteps-((fadeSteps - darkTime)/2));
        Serial.print(increment[i]);
        Serial.print("        ");     
    }
    Serial.println();
    Serial.print("steps down/up = ");
    Serial.println((fadeSteps - darkTime)/2);
        
        
    for (f=((fadeSteps - darkTime)/2)+ darkTime; f < fadeSteps; f++){
        if (debug){Serial.print("calculating....          ");}
            for (int i = 0; i < 3; i++){  
                fade[i]=current[i]+increment[i]; 
                fadeMatrix[i][f]=fade[i];
                current[i]=fade[i];
            
                if (debug){
                    Serial.print(fade[i]);
                    Serial.print("          ");
                }
            }
            if (debug){Serial.println();
            }
        }
        //Serial.println(millis());
        break;
        
    case 2:
        //Serial.println(millis());
        Serial.print("Increment          ");
        for(int i=0;i<3;i++){
            increment[i]=(target[i]-current[i])/fadeSteps; 
            Serial.print(increment[i]);
            Serial.print("          ");
            fadeMatrix[i][0]=current[i];
        
        }
        Serial.println();
        
        for(f=1;f<fadeSteps;f++){
            if(debug){Serial.print("calculating.....     ");}
            for (int i=0;i<3;i++){
                fade[i]=current[i]+increment[i]; 
                fadeMatrix[i][f]=fade[i];
                current[i]=fade[i];
                if(debug){
                    Serial.print(fade[i]);
                    Serial.print("          ");
                }
            }  
        
            if(debug){
                Serial.println();
            }
        }
    } 
    //Serial.println(millis());
    //-----------do the fade ----------------
    // led goes low
    digitalWrite(ledPin, LOW);
    for (int f = 0; f < fadeSteps; f++){
        //increment the RGB values by one step 
        if (debug){
        Serial.print("fading....         ");
        }
        for (int i = 0; i < 3; i++){
            //fade[i] = fade[i] + increment[i];             // update fade[]
            delay(fadeStats[0]*fadeStats[0]);               //delay between each colour = 3 times per step
            analogWrite(outRGB[i], fadeMatrix[i][f]);
            if(debug){
                Serial.print(fadeMatrix[i][f]);
                Serial.print("        ");
            }
            //current[i] = fade[i];
        }                        
        Serial.println("");
    }
    //fade is finished, should be the values we were looking for, but if not.....
    Serial.println("complete           ");
    digitalWrite(ledPin, HIGH);                                //fade complete
    
    //for (int i = 0; i < 3; i++){     //set the current value for the next fade
    //     current[i] = target[i];      // make the old target the new current.
    //     analogWrite(outRGB[i], current[i]); //write to the LEDs to clean up any fade problems 
    //}  
} //######################## end of doFade  ########################################//

//**********************************************************************************//
//*     the loop function runs over and over again until power down or reset       *//
//**********************************************************************************//
void loop() {
//-------------------------------- read from serial -----------------------------
// obloleted by i2c
    while (Serial.available()) {
        // get the new byte:
        char inChar = (char)Serial.read(); 
        // if the incoming character is a newline, check if it is for this device ID
        // then set a flag so the main loop can do something about it: 
        inputString += inChar;
        if (inChar == '\n') {
            Serial.print("INPUT :");
            Serial.println(inputString);
            // read the uCID
            uCIDstring=inputString.substring(0,(EEidReserved));
            Serial.print("Message For uCID ");
            
            // just checking the third digit for now - up to 10 lights
            
            tempString=uCIDstring.substring(2);
            uCIDtemp=tempString.toInt();
            Serial.print(uCIDtemp);
            Serial.print(" and I'm ");
            Serial.println(uCID);
            if(uCIDtemp==uCID){
                Serial.println("That's me!");
                inputRec();
            }
            else {
                Serial.println("Not me :(");
                //prefix=0;
                inputString=0;  
            }
        }
    }

//++++++++++++++++++++++++++++++++ start of autoFade==1 ++++++++++++++++++++++++++++++++++//
    if(autoFade==1 && mode==1){
        Serial.println("autoFade on");
        for (int i = 0; i < 3; i++){            //set the current value for the next fade
          current[i] = preset[p][i];            // make the old target preset the new current preset.
        }       
        p++;                                    // increase the preset number
        if(p>pMax){                             // or start at the first preset
                 Serial.println("---------------------pMax acheived--------------------------");
                 p=1;
        } 
        
        for(int i = 0; i < 3; i++){             //make the new preset the target
          target[i]=int(preset[p][i]);
        }
            
        Serial.print("Fade Duration........");
        Serial.println(fadeStats[0]*fadeSteps);                 //fade duration
        
        // move this into doFade?
        //Serial.print("Hold on current colour......");
        //Serial.println(holdTime*fadeStats[1]);                //hold duration
        
        Serial.println("");        
        delay(fadeStats[1]^2);                                  //hold duration
  
        // call doFade and then reset fadeNow to 0
        doFade();        
  }
//+++++++++++++++++++++++++++++++++ end of autoFade==1 ++++++++++++++++++++++++++++++++++//
} //---------------------------------- end of loop --------------------------------------//
